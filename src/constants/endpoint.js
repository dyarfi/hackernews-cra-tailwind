const URL = 'https://hacker-news.firebaseio.com/v0/'

const ENDPOINTS = {
  STORY: 'item/',
  NEWSTORIES: 'newstories.json?print=pretty',
  ASKSTORIES: 'askstories.json?print=pretty',
  JOBSTORIES: 'jobstories.json?print=pretty',
  SHOWSTORIES: 'showstories.json?print=pretty',
  USER: '.json?print=pretty',
  MOCK: 'item/8863.json?print=pretty'
}

const API_URL_STORY = `${URL}${ENDPOINTS.STORY}`
const API_URL_ITEM = (ITEM) =>
  `${URL}${ENDPOINTS.STORY}${ITEM}.json?print=pretty'`
const API_URL_NEWSTORIES = `${URL}${ENDPOINTS.NEWSTORIES}`
const API_URL_ASKSTORIES = `${URL}${ENDPOINTS.ASKSTORIES}`
const API_URL_JOBSTORIES = `${URL}${ENDPOINTS.JOBSTORIES}`
const API_URL_SHOWSTORIES = `${URL}${ENDPOINTS.SHOWSTORIES}`
const API_URL_USER = (ITEM) => `${URL}user/${ITEM}${ENDPOINTS.USER}`

const ENDPOINT = {
  API_URL: URL,
  API_URL_MOCK: `${URL}${ENDPOINTS.MOCK}`,
  API_URL_STORY,
  API_URL_NEWSTORIES,
  API_URL_ASKSTORIES,
  API_URL_JOBSTORIES,
  API_URL_SHOWSTORIES,
  API_URL_USER,
  API_URL_ITEM
}

export default ENDPOINT
