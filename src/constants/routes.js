const ROUTES = [
  { path: '/', name: 'Home', component: './App', nav: true },
  { path: '/ask', name: 'Ask', component: './pages/Ask', nav: true },
  { path: '/jobs', name: 'Jobs', component: './pages/Jobs', nav: true },
  { path: '/show', name: 'Show', component: './pages/Show', nav: true },
  {
    path: '/comments/:id',
    name: 'Comments',
    component: './pages/Comments',
    nav: false
  }
]

export default ROUTES
