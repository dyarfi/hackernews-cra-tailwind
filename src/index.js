import React from 'react'
import ReactDOM from 'react-dom'
import { Route, BrowserRouter as Router, Switch } from 'react-router-dom'

import './index.css'
import reportWebVitals from './reportWebVitals'

import App from './App'
import Page404 from 'pages/404'

import ROUTES from 'constants/routes'

ReactDOM.render(
  <React.StrictMode>
    <Router>
      <Switch>
        <Route exact path="/" component={App} />
        {ROUTES.map((route) => {
          return (
            <Route
              key={route.path}
              exact
              path={route.path}
              component={require(`${route.component}`).default}
            />
          )
        })}
        <Route component={Page404} />
      </Switch>
    </Router>
  </React.StrictMode>,
  document.getElementById('root')
)

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals()
