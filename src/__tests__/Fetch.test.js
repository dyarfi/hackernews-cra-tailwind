import ENDPOINT from '../constants/endpoint'
import { fetchData, fetchDataAPI } from '../utils'

jest.setTimeout(10000)

// should printout api url
test('should printout api url', () => console.log(ENDPOINT.API_URL))

// should connect to api url mock
test('should connect to api url mock', async () =>
  await fetch(ENDPOINT.API_URL_MOCK).then(async (res) => await res.json()))

// should connect to api url mock fetchData
test('should connect to api url mock fetchData', async () =>
  await fetchData(ENDPOINT.API_URL_MOCK))

// should return response from api url story
test('should return response from api url story', async () =>
  await fetchDataAPI({ url: ENDPOINT.API_URL_MOCK }))

// should return response from api url newstories
test('should return response from api url newstories', async () =>
  await fetchDataAPI({
    url: ENDPOINT.API_URL_NEWSTORIES
  }))

// should return response from api url askstories
test('should return response from api url askstories', async () =>
  await fetchDataAPI({ url: ENDPOINT.API_URL_ASKSTORIES }))
