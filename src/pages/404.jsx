import React from 'react'

function Page404({ location }) {
  return (
    <div className="container">
      <div>
        <div className="container my-5 py-5">
          <h2 className="mx-auto text-white text-xl">
            No match found for{' '}
            <code className="mr-2 text-gray-400">{location.pathname}</code>
            <a href="/" className="d-block h4 my-2 font-semibold">
              [back to home]
            </a>
          </h2>
        </div>
      </div>
    </div>
  )
}

export default Page404
