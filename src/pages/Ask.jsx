import React, { useState, useEffect } from 'react'

/* utils */
import { fetchDataAPI } from 'utils'

/* constants */
import ENDPOINT from 'constants/endpoint'

/* components */
import Card from 'components/Card'
import Loader from 'components/Loader'
import Navigation from 'components/Navigation'

function Ask() {
  const [list, setList] = useState(null)

  useEffect(() => initial(), [])

  async function initial() {
    const response = await fetchDataAPI({
      url: ENDPOINT.API_URL_ASKSTORIES
    })
    setList(response)
  }

  return (
    <div className="container mx-auto text-center">
      {!list ? (
        <Loader />
      ) : (
        <>
          <Navigation />
          <Card item={list} />
        </>
      )}
    </div>
  )
}

export default Ask
