import React, { useState, useEffect } from 'react'
import { useParams, Redirect } from 'react-router'

/* utils */
import { fetchData, fetchDataAPI } from 'utils'

/* constants */
import ENDPOINT from 'constants/endpoint'

/* components */
import Card from 'components/Card'
import Loader from 'components/Loader'
import Navigation from 'components/Navigation'

function Comments() {
  const params = useParams()
  const [detail, setDetail] = useState(null)
  const [list, setList] = useState(null)

  useEffect(() => {
    async function initial() {
      const detail = await fetchData(ENDPOINT.API_URL_ITEM(params.id))
      setDetail(detail)
    }
    async function initialComments() {
      const children = await fetchDataAPI({
        url: ENDPOINT.API_URL_ITEM(params.id)
      })
      setList(children)
    }
    initial()
    initialComments()
  }, [params.id])

  return (
    <div className="container mx-auto text-center">
      {!list ? (
        <Loader />
      ) : list.error ? (
        <Redirect to="/pagenotfound" />
      ) : (
        <>
          <Navigation />
          <div className="container mx-auto p-2">
            <article className="card">
              <h1 className="font-black text-4xl">{detail?.title}</h1>
              <div className="flex flex-row">
                <div className="mr-3">By: {detail?.by}</div>
                <div className="mr-3">
                  Created: {new Date(detail?.time * 1000).toLocaleString()}
                </div>
              </div>
            </article>
          </div>
          <Card item={list} type="wide" />
        </>
      )}
    </div>
  )
}

export default Comments
