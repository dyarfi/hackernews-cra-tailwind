import React from 'react'
import ROUTES from 'constants/routes'
import { NavLink } from 'react-router-dom'

function Navigation() {
  return (
    <ul className="flex flex-row text-white text-2xl lg:text-4xl font-black px-2 text-right">
      {ROUTES.map(
        (route) =>
          route.nav && (
            <li key={route.path} className="w-full">
              <NavLink to={route.path}>{route.name}</NavLink>
            </li>
          )
      )}
    </ul>
  )
}

export default Navigation
