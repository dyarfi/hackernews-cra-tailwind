import React from 'react'

function Loader() {
  return (
    <div className="flex flex-col min-h-screen align-center justify-center h-full overflow-hidden preloader">
      <h1 className="text-8xl font-black text-yellow-600 logo">HN</h1>
    </div>
  )
}

export default Loader
