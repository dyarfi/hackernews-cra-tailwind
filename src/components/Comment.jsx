import React from 'react'

function Comment({ item }) {
  return (
    <div className="flex flex-row flex-wrap">
      {item && (
        <div className="w-full sm:w-1/2 xl:w-1/3 p-2">
          <div>{item.by} </div>
          <div>{item.text} </div>
        </div>
      )}
    </div>
  )
}

export default Comment
