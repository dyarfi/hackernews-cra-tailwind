import React from 'react'
import { NavLink } from 'react-router-dom'
import { getHostName } from 'utils'

function Card({ item, type = '' }) {
  const typeCard =
    type === 'wide' ? `w-full  p-2` : `w-full sm:w-1/2 xl:w-1/3 p-2`

  return (
    <div className="flex flex-row flex-wrap">
      {item &&
        item.length > 0 &&
        item.map((obj) => (
          <div className={typeCard} key={`${obj.id}-${obj.by}`}>
            <article className="card">
              <header>
                <a
                  href={obj?.url ? obj.url : '#'}
                  className="hover:text-gray-500"
                  rel="noreferrer"
                  target="_blank"
                >
                  {obj?.type === 'comment' ? (
                    <h5
                      className="font-semibold text-lg leading-none mb-2"
                      dangerouslySetInnerHTML={{ __html: obj?.text }}
                    ></h5>
                  ) : (
                    <h2 className="font-bold text-xl leading-none mb-2">
                      {obj?.title}
                    </h2>
                  )}
                </a>
              </header>
              <div className="text-sm">
                {obj?.by && `@${obj.by}`}
                {obj?.score > 0 && ` | #${obj.score}`}
                {obj?.descendants > 0 && (
                  <NavLink to={`/comments/${obj.id}`}>
                    {` | ${obj?.descendants} comments`}
                  </NavLink>
                )}
                {obj?.type && ` | ?${obj?.type}`}
              </div>
              <footer className="text-sm">
                time:<span>{new Date(obj.time * 1000).toLocaleString()}</span>
                {obj?.url && ` (${getHostName(obj?.url)})`}
              </footer>
            </article>
          </div>
        ))}
    </div>
  )
}

export default Card
