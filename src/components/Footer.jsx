import React from 'react'
import { NavLink } from 'react-router-dom'

function Footer() {
  return (
    <ul className="text-white text-lg font-black px-2 text-center mt-1 mb-2">
      <li>
        <NavLink to="https://dyarfi@bitbucket.org/dyarfi/hackernews-cra-tailwind.git">
          Git Source
        </NavLink>
      </li>
    </ul>
  )
}

export default Footer
