import ENDPOINT from '../constants/endpoint'

const axios = require('axios').default

/**
 * @param  {string} url=''
 * @param  {object} options=null
 */
function fetchData(url = '', options = null) {
  try {
    const items = axios({ url, ...options })
      .then((item) => item.data)
      .catch(function (error) {
        return { error: { items: error } }
      })
    return items
  } catch (error) {
    throw new Error(error)
  }
}

/**
 * @param  {string} url=""
 * @param  {object} options=null
 */
function fetchDataAPI({ url = '', options = null }) {
  try {
    const result = axios
      .get(url, options)
      .then((res) => {
        const list = res.data.length > 0 ? [...res.data] : [...res.data?.kids]
        const listItem = list.slice(0, 90).map((item) => {
          const items = fetchData(
            `${ENDPOINT.API_URL_STORY}${
              typeof item === 'object' ? item.kids : item
            }.json`
          )
          return items
        })
        const response = Promise.all(listItem)
        return response
      })
      .catch(function (error) {
        return { error: { result: error } }
      })
    return result
  } catch (error) {
    throw new Error(error)
  }
}

/**
 * @param  {string} url=''
 */
function getHostName(url = '') {
  var match = url.match(/:\/\/(www[0-9]?\.)?(.[^/:]+)/i)
  if (
    match != null &&
    match.length > 2 &&
    typeof match[2] === 'string' &&
    match[2].length > 0
  ) {
    return match[2]
  } else {
    return null
  }
}

export { fetchData, fetchDataAPI, getHostName }
